from typing import Final

import day_04
from helpers import run_all_tests, input_for

example_input: Final = """
Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11
""".strip()
example_lines: Final = example_input.split('\n')


class TestCard:
    def test_parse(self):
        first_line: Final = example_lines[0]
        card: Final = day_04.Card.parse(first_line)
        assert card.card_id == 1
        assert card.winning_numbers == {41, 48, 83, 86, 17}
        assert card.selected_numbers == {83, 86, 6, 31, 17, 9, 48, 53}

    def test_matching_numbers(self):
        cards: Final = map(day_04.Card.parse, example_lines)
        assert list(map(lambda c: c.matching_numbers, cards)) == [
            {48, 83, 17, 86},
            {32, 61},
            {1, 21},
            {84},
            set(),
            set(),
        ]

    def test_points(self):
        cards: Final = map(day_04.Card.parse, example_lines)
        assert list(map(lambda c: c.points, cards)) == [
            8,
            2,
            2,
            1,
            0,
            0,
        ]

    def test_won_copies(self):
        cards: Final = map(day_04.Card.parse, example_lines)
        assert list(map(lambda c: list(c.won_copies), cards)) == [
            [2, 3, 4, 5],
            [3, 4],
            [4, 5],
            [5],
            [],
            [],
        ]


class TestScratchcardGame:
    def test_play(self):
        game: Final = day_04.ScratchcardGame.parse(example_lines)
        game.play()
        assert game.num_total_copies == 30


def test_solution_1():
    input_lines: Final = input_for(day_04)
    game: Final = day_04.ScratchcardGame.parse(input_lines)
    assert len(game.cards) == 223
    assert game.point_sum == 32001


def test_solution_2():
    input_lines: Final = input_for(day_04)
    game: Final = day_04.ScratchcardGame.parse(input_lines)
    game.play()
    assert game.num_total_copies == 5037841


if __name__ == "__main__":
    run_all_tests()
