from typing import Final

import day_02
from helpers import run_all_tests, input_for

example_input_1: Final = """
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
""".strip()
example_lines_1: Final = example_input_1.split('\n')

example_input_2: Final = """
Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green
""".strip()
example_lines_2: Final = example_input_2.split('\n')


class TestBagContent:
    def test_cube_power(self):
        bag_content: Final = day_02.BagContent(
            num_blue=14,
            num_green=13,
            num_red=12,
        )
        assert bag_content.cube_power == 2184


class TestGameRound:
    def test_parse(self):
        first_line: Final = example_lines_1[0].removeprefix('Game 1: ')
        assert list(map(day_02.GameRound.parse, first_line.split('; '))) == [
            day_02.GameRound(num_blue=3, num_green=0, num_red=4),
            day_02.GameRound(num_blue=6, num_green=2, num_red=1),
            day_02.GameRound(num_blue=0, num_green=2, num_red=0),
        ]

    def test_is_possible(self):
        game_round: Final = day_02.GameRound(num_blue=6, num_green=2, num_red=1)
        assert game_round.is_possible(day_02.BagContent(num_blue=16, num_green=22, num_red=31))
        assert game_round.is_possible(day_02.BagContent(num_blue=6, num_green=2, num_red=1))
        assert not game_round.is_possible(day_02.BagContent(num_blue=4, num_green=2, num_red=1))
        assert not game_round.is_possible(day_02.BagContent(num_blue=6, num_green=0, num_red=1))
        assert not game_round.is_possible(day_02.BagContent(num_blue=6, num_green=2, num_red=0))


class TestGame:
    def test_parse(self):
        games: Final = list(map(day_02.Game.parse, example_lines_1))
        assert list(map(lambda game: game.game_id, games)) == list(range(1, 6))
        assert (
            list(map(lambda game: len(game.game_rounds), games))
            == list(map(lambda line: line.count("; ") + 1, example_lines_1))
        )

    def test_required_bag_content(self):
        games: Final = list(map(day_02.Game.parse, example_lines_2))
        assert list(map(lambda game: game.required_bag_content, games)) == [
            day_02.BagContent(num_blue=6, num_green=2, num_red=4),
            day_02.BagContent(num_blue=4, num_green=3, num_red=1),
            day_02.BagContent(num_blue=6, num_green=13, num_red=20),
            day_02.BagContent(num_blue=15, num_green=3, num_red=14),
            day_02.BagContent(num_blue=2, num_green=3, num_red=6),
        ]


def test_solution_1():
    bag_content: Final = day_02.BagContent(
        num_blue=14,
        num_green=13,
        num_red=12,
    )
    input_lines: Final = input_for(day_02)
    all_games: Final = list(map(day_02.Game.parse, input_lines))
    possible_games: Final = [
        game
        for game
        in all_games
        if all(
            game_round.is_possible(bag_content)
            for game_round
            in game.game_rounds
        )
    ]
    assert sum(
        game.game_id
        for game
        in possible_games
    ) == 2149


def test_solution_2():
    input_lines: Final = input_for(day_02)
    all_games: Final = list(map(day_02.Game.parse, input_lines))
    assert sum(
        game.required_bag_content.cube_power
        for game
        in all_games
    ) == 71274


if __name__ == "__main__":
    run_all_tests()
