import re
import string
from dataclasses import dataclass
from functools import cached_property
from typing import Final

number_pattern: Final = re.compile(r'(?P<value>\d+)')
star_pattern: Final = re.compile(r'(?P<value>\*)')


@dataclass
class SpecialField:
    x: int
    y: int
    length: int

    def __hash__(self) -> int:
        return hash((
            self.__class__,
            self.x,
            self.y,
            self.length,
        ))

    @cached_property
    def neighbor_coordinates(self) -> list[tuple[int, int]]:
        return (
            [(x, self.y - 1) for x in range(self.x - 1, self.x + self.length + 1)]
            + [(x, self.y) for x in (self.x - 1, self.x + self.length)]
            + [(x, self.y + 1) for x in range(self.x - 1, self.x + self.length + 1)]
        )

    @staticmethod
    def from_match(y: int, match: re.Match) -> 'SpecialField':
        span = match.span('value')
        return SpecialField(
            x=span[0],
            y=y,
            length=span[1] - span[0],
        )


@dataclass
class EngineSchematicNumber(SpecialField):
    value: int

    @staticmethod
    def from_match(y: int, match: re.Match) -> 'EngineSchematicNumber':
        span = match.span('value')
        return EngineSchematicNumber(
            x=span[0],
            y=y,
            length=span[1] - span[0],
            value=int(match.group('value')),
        )


@dataclass
class Gear(SpecialField):
    parts: tuple[EngineSchematicNumber, EngineSchematicNumber]

    @property
    def gear_ratio(self) -> int:
        return self.parts[0].value * self.parts[1].value


@dataclass
class EngineSchematic:
    rows: list[str]

    def find_all_patterns(self, pattern: re.Pattern) -> list[tuple[int, re.Match]]:
        return [
            (y, match)
            for y, line
            in enumerate(self.rows)
            for match
            in pattern.finditer(line)
        ]

    @cached_property
    def all_numbers(self) -> list[EngineSchematicNumber]:
        return [
            EngineSchematicNumber.from_match(y, match)
            for y, match
            in self.find_all_patterns(number_pattern)
        ]

    @cached_property
    def part_numbers(self) -> list[EngineSchematicNumber]:
        return [
            number
            for number
            in self.all_numbers
            if any(
                self.has_symbol(x, y)
                for x, y
                in number.neighbor_coordinates
            )
        ]

    @cached_property
    def all_stars(self) -> list[SpecialField]:
        return [
            SpecialField.from_match(y, match)
            for y, match
            in self.find_all_patterns(star_pattern)
        ]

    @cached_property
    def gears(self) -> list[Gear]:
        stars_by_coordinate = dict(
            ((star.x, star.y), star)
            for star
            in self.all_stars
        )
        star_parts = dict(
            (star, [])
            for star
            in self.all_stars
        )
        for number in self.part_numbers:
            for x, y in number.neighbor_coordinates:
                if (x, y) in stars_by_coordinate:
                    star = stars_by_coordinate[(x, y)]
                    star_parts[star].append(number)

        return [
            Gear(
                parts=(parts[0], parts[1]),
                **vars(star)
            )
            for star, parts
            in star_parts.items()
            if len(parts) == 2
        ]

    @property
    def part_sum(self):
        return sum(
            number.value
            for number
            in self.part_numbers
        )

    @property
    def gear_ratio_sum(self):
        return sum(
            gear.gear_ratio
            for gear
            in self.gears
        )

    def is_in_bounds(self, x: int, y: int) -> bool:
        return (
            0 <= y < len(self.rows)
            and 0 <= x < len(self.rows[y])
        )

    def field(self, x: int, y: int) -> str | None:
        if not self.is_in_bounds(x, y):
            return None

        return self.rows[y][x]

    def has_digit(self, x: int, y: int) -> bool:
        value: Final = self.field(x, y)
        return value and value in string.digits

    def has_symbol(self, x: int, y: int) -> bool:
        if self.has_digit(x, y):
            return False

        value: Final = self.field(x, y)
        return value and value != '.'

    def adjacent_symbols(self, number: EngineSchematicNumber):
        return [
            self.rows[y][x]
            for x, y
            in number.neighbor_coordinates
            if self.has_symbol(x, y)
        ]

    @staticmethod
    def parse(lines) -> 'EngineSchematic':
        return EngineSchematic(rows=lines)
