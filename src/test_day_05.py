from functools import reduce
from typing import Final

import day_05
from helpers import run_all_tests, input_for

example_input: Final = """
seeds: 79 14 55 13

seed-to-soil map:
50 98 2
52 50 48

soil-to-fertilizer map:
0 15 37
37 52 2
39 0 15

fertilizer-to-water map:
49 53 8
0 11 42
42 0 7
57 7 4

water-to-light map:
88 18 7
18 25 70

light-to-temperature map:
45 77 23
81 45 19
68 64 13

temperature-to-humidity map:
0 69 1
1 0 69

humidity-to-location map:
60 56 37
56 93 4
""".strip()
example_lines: Final = example_input.split('\n')


class TestAlmanacMapping:
    def test_parse(self):
        mapping: Final = day_05.AlmanacMapping.parse(example_lines[2:5])
        assert mapping.source_category == 'seed'
        assert mapping.destination_category == 'soil'
        assert mapping.ranges == [
            (range(98, 100), range(50, 52)),
            (range(50, 98), range(52, 100)),
        ]

    def test_call(self):
        mapping: Final = day_05.AlmanacMapping.parse(example_lines[2:5])
        assert mapping(11) == 11
        assert mapping(50) == 52
        assert mapping(98) == 50
        assert mapping(100) == 100


class TestAlmanac:
    def test_parse(self):
        almanac: Final = day_05.Almanac.parse(example_lines)
        assert almanac.seed_numbers == [79, 14, 55, 13]
        assert list(map(
            lambda m: (m.source_category, m.destination_category),
            almanac.mappings
        )) == [
                   ('seed', 'soil'),
                   ('soil', 'fertilizer'),
                   ('fertilizer', 'water'),
                   ('water', 'light'),
                   ('light', 'temperature'),
                   ('temperature', 'humidity'),
                   ('humidity', 'location'),
               ]

    def test_seed_ranges(self):
        almanac: Final = day_05.Almanac.parse(example_lines)
        assert almanac.seed_ranges == [
            range(79, 79 + 14),
            range(55, 55 + 13),
        ]

    def test_apply_mappings(self):
        almanac: Final = day_05.Almanac.parse(example_lines)

        seed_numbers = almanac.seed_numbers
        locations = almanac.apply_mappings(seed_numbers)
        assert locations == [82, 43, 86, 35]
        assert min(locations) == 35

        seed_numbers = reduce(lambda a, b: a + b, map(list, almanac.seed_ranges), [])
        locations = almanac.apply_mappings(seed_numbers)
        assert min(locations) == 46


def test_solution_1():
    input_lines: Final = input_for(day_05)
    almanac: Final = day_05.Almanac.parse(input_lines)
    locations: Final = almanac.apply_mappings(almanac.seed_numbers)
    assert min(locations) == 57075758


if __name__ == "__main__":
    run_all_tests()
