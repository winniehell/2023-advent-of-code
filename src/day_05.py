import re
from dataclasses import dataclass
from typing import Final

initial_seed_pattern: Final = re.compile(r'seeds: (?P<seed_numbers>[0-9 ]+)$')
mapping_title_pattern: Final = re.compile(r'^(?P<source_category>[a-z]+)-to-(?P<destination_category>[a-z]+) map:$')


@dataclass(frozen=True)
class AlmanacMapping:
    destination_category: str
    source_category: str
    ranges: list[tuple[range, range]]

    def __call__(self, value: int):
        for source_range, destination_range in self.ranges:
            if value in source_range:
                return destination_range[source_range.index(value)]

        return value

    @staticmethod
    def parse_range_line(line: str) -> tuple[range, range]:
        destination_start, source_start, length = map(int, line.split(' '))
        return (
            range(source_start, source_start + length),
            range(destination_start, destination_start + length),
        )

    @staticmethod
    def parse(lines: list[str]) -> 'AlmanacMapping':
        title_match: Final = mapping_title_pattern.match(lines[0])
        return AlmanacMapping(
            destination_category=title_match.group('destination_category'),
            source_category=title_match.group('source_category'),
            ranges=list(map(AlmanacMapping.parse_range_line, lines[1:]))
        )


@dataclass(frozen=True)
class Almanac:
    seed_numbers: list[int]
    mappings: list[AlmanacMapping]

    @property
    def seed_ranges(self):
        return [
            range(
                self.seed_numbers[i],
                self.seed_numbers[i] + self.seed_numbers[i + 1],
            )
            for i
            in range(0, len(self.seed_numbers), 2)
        ]

    def mapping_by_category(self, category: str) -> AlmanacMapping:
        for mapping in self.mappings:
            if mapping.source_category == category:
                return mapping
        raise ValueError(category)

    def apply_mappings(self, seed_numbers: list[int]) -> list[int]:
        category = 'seed'
        values = seed_numbers
        for i in range(len(self.mappings)):
            mapping = self.mapping_by_category(category)
            category = mapping.destination_category
            values = map(mapping, values)
        return list(values)

    @staticmethod
    def parse(lines: list[str]) -> 'Almanac':
        block_indices = [
            i + 1
            for i
            in range(len(lines))
            if lines[i] == ''
        ]
        return Almanac(
            seed_numbers=list(map(
                int,
                initial_seed_pattern
                .match(lines[0])
                .group('seed_numbers')
                .split(' ')
            )),
            mappings=[
                AlmanacMapping.parse(lines=lines[start:end - 1])
                for start, end
                in zip(
                    block_indices,
                    block_indices[1:] + [len(lines)],
                )
            ],
        )
