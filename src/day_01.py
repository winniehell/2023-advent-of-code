import re

digit_pattern = re.compile(r'\d')
digit_words = (
    'zero',
    'one',
    'two',
    'three',
    'four',
    'five',
    'six',
    'seven',
    'eight',
    'nine',
)
digit_word_pattern = re.compile('(?=(' + '|'.join(digit_words) + '))')


def only_digits(line: str) -> list[str]:
    return digit_pattern.findall(line)


def parse_words(line: str) -> str:
    result = line
    for i, match in enumerate(digit_word_pattern.finditer(line)):
        pos = match.span(0)[0] + i
        result = result[:pos] + str(digit_words.index(match.group(1))) + result[pos:]
    return result


def calibration_value(line: str) -> str:
    digits = only_digits(line)
    return digits[0] + digits[-1]


def calibration_value_sum(lines: list[str]) -> int:
    all_calibration_values = map(calibration_value, lines)
    return sum(map(int, all_calibration_values))
