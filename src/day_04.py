import re
from dataclasses import dataclass
from typing import Final

from logger import create_default_logger

card_pattern: Final = re.compile(
    r'^Card +(?P<card_id>\d+): (?P<winning_numbers>[\d ]+) \| (?P<selected_numbers>[\d ]+)$'
)

log = create_default_logger(__file__)


@dataclass
class Card:
    card_id: int
    selected_numbers: set[int]
    winning_numbers: set[int]
    num_copies: int = 1

    @property
    def matching_numbers(self) -> set[int]:
        return self.winning_numbers.intersection(self.selected_numbers)

    @property
    def points(self) -> int:
        if len(self.matching_numbers) < 1:
            return 0

        return 2 ** (len(self.matching_numbers) - 1)

    @property
    def won_copies(self) -> range:
        return range(
            self.card_id + 1,
            self.card_id + 1 + len(self.matching_numbers),
        )

    @staticmethod
    def parse(line: str) -> 'Card':
        match: Final = card_pattern.match(line)
        return Card(
            card_id=int(match.group('card_id')),
            selected_numbers=set(map(int, match.group('selected_numbers').split())),
            winning_numbers=set(map(int, match.group('winning_numbers').split())),
        )


@dataclass
class ScratchcardGame:
    cards: list[Card]

    @property
    def point_sum(self) -> int:
        return sum(
            card.points
            for card
            in self.cards
        )

    @property
    def num_total_copies(self) -> int:
        return sum(
            card.num_copies
            for card
            in self.cards
        )

    def play(self) -> None:
        for card in self.cards:
            log.info(f'processing card {card.card_id}')
            for card_id in card.won_copies:
                if card_id > len(self.cards):
                    log.warning(f'skipping card {card_id} > {len(self.cards)}')
                    continue

                new_card = self.cards[card_id - 1]
                assert new_card.card_id == card_id
                log.info(f'adding {card.num_copies} copies of card {card_id}')
                new_card.num_copies += card.num_copies

    @staticmethod
    def parse(lines: list[str]) -> 'ScratchcardGame':
        return ScratchcardGame(
            cards=list(map(Card.parse, lines))
        )
