import re
from dataclasses import dataclass
from math import prod
from typing import Final

game_line_pattern: Final = re.compile(r'^Game (?P<game_id>\d+): (?P<rounds>.+)$')
game_round_pattern: Final = re.compile(
    r'^(('
    r'(?P<num_blue>\d+) blue'
    r'|(?P<num_green>\d+) green'
    r'|(?P<num_red>\d+) red'
    r')(, |$))+'
)


@dataclass
class BagContent:
    num_blue: int
    num_green: int
    num_red: int

    @property
    def cube_power(self):
        return prod((
            self.num_blue,
            self.num_green,
            self.num_red,
        ))


@dataclass
class GameRound:
    num_blue: int = 0
    num_green: int = 0
    num_red: int = 0

    @staticmethod
    def parse(line_part: str) -> 'GameRound':
        match: Final = game_round_pattern.match(line_part)
        return GameRound(
            **dict(
                (key, int(value) if value else 0)
                for key, value
                in match.groupdict().items()
            )
        )

    def is_possible(self, bag_content: BagContent) -> bool:
        return all(
            getattr(self, key) <= value
            for key, value
            in vars(bag_content).items()
        )


@dataclass
class Game:
    game_id: int
    game_rounds: list[GameRound]

    @property
    def required_bag_content(self) -> BagContent:
        return BagContent(
            num_blue=max(game_round.num_blue for game_round in self.game_rounds),
            num_green=max(game_round.num_green for game_round in self.game_rounds),
            num_red=max(game_round.num_red for game_round in self.game_rounds),
        )

    @staticmethod
    def parse(line: str) -> 'Game':
        line_match: Final = game_line_pattern.match(line)
        return Game(
            game_id=int(line_match.group('game_id')),
            game_rounds=list(
                map(GameRound.parse, line_match.group('rounds').split('; '))
            ),
        )
