from typing import Final

import day_01
from helpers import input_for, run_all_tests

example_input_1: Final = """
1abc2
pqr3stu8vwx
a1b2c3d4e5f
treb7uchet
""".strip()
example_lines_1: Final = example_input_1.split('\n')

example_input_2: Final = """
two1nine
eightwothree
abcone2threexyz
xtwone3four
4nineeightseven2
zoneight234
7pqrstsixteen
""".strip()
example_lines_2: Final = example_input_2.split('\n')


def test_only_digits():
    result = list(map(day_01.only_digits, example_lines_1))
    assert result == [
        ['1', '2'],
        ['3', '8'],
        ['1', '2', '3', '4', '5'],
        ['7']
    ]


def test_parse_words():
    result = list(map(day_01.parse_words, example_lines_2))
    assert result == [
        '2two19nine',
        '8eigh2two3three',
        'abc1one23threexyz',
        'x2tw1one34four',
        '49nine8eight7seven2',
        'z1on8eight234',
        '7pqrst6sixteen',
    ]


def test_calibration_value():
    result = list(map(day_01.calibration_value, example_lines_1))
    assert result == [
        '12',
        '38',
        '15',
        '77',
    ]


def test_calibration_value_sum():
    assert day_01.calibration_value_sum(example_lines_1) == 142


def test_solution_1():
    input_lines: Final = input_for(day_01)
    assert day_01.calibration_value_sum(input_lines) == 55090


def test_solution_2():
    input_lines = input_for(day_01)
    input_lines = list(map(day_01.parse_words, input_lines))
    assert day_01.calibration_value_sum(input_lines) == 54845


if __name__ == "__main__":
    run_all_tests()
