from types import SimpleNamespace
from typing import Final, cast

import day_03
from helpers import run_all_tests, input_for

example_input_1: Final = """
467..114..
...*......
..35..633.
......#...
617*......
.....+.58.
..592.....
......755.
...$.*....
.664.598..
""".strip()
example_lines_1: Final = example_input_1.split('\n')


class TestSpecialField:
    match = day_03.star_pattern.search('abcd*ef')

    def test_from_match(self):
        number: Final = day_03.SpecialField.from_match(19, self.match)
        assert number == day_03.SpecialField(
            y=19,
            x=4,
            length=1,
        )

    def test_neighbor_coordinates(self):
        number: Final = day_03.SpecialField.from_match(23, self.match)
        assert len(number.neighbor_coordinates) == 8
        assert number.neighbor_coordinates == [
            (3, 22),
            (4, 22),
            (5, 22),
            (3, 23),
            (5, 23),
            (3, 24),
            (4, 24),
            (5, 24),
        ]


class TestEngineSchematicNumber:
    match = day_03.number_pattern.search('abc123def')

    def test_from_match(self):
        number: Final = day_03.EngineSchematicNumber.from_match(42, self.match)
        assert number == day_03.EngineSchematicNumber(
            y=42,
            x=3,
            length=3,
            value=123,
        )

    def test_neighbor_coordinates(self):
        number: Final = day_03.EngineSchematicNumber.from_match(53, self.match)
        assert len(number.neighbor_coordinates) == 2 * number.length + 2 * 3
        assert number.neighbor_coordinates == (
            [(x, 52) for x in range(2, 7)]
            + [(x, 53) for x in (2, 6)]
            + [(x, 54) for x in range(2, 7)]
        )


class TestGear:
    def test_gear_ratio(self):
        dummy_part_1 = cast(day_03.EngineSchematicNumber, SimpleNamespace(value=19))
        dummy_part_2 = cast(day_03.EngineSchematicNumber, SimpleNamespace(value=21))
        gear = day_03.Gear(
            x=1,
            y=2,
            length=3,
            parts=(dummy_part_1, dummy_part_2)
        )
        assert gear.gear_ratio == 399


class TestEngineSchematic:
    def test_has_symbol(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.has_symbol(x=3, y=1)
        assert schematic.has_symbol(x=6, y=3)
        assert not schematic.has_symbol(x=0, y=0)
        assert not schematic.has_symbol(x=1, y=1)
        assert not schematic.has_symbol(x=-1, y=-1)

    def test_adjacent_symbols(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert (
            [
                (number.value, schematic.adjacent_symbols(number))
                for number
                in schematic.all_numbers
            ]
            == [
                (467, ['*']),
                (114, []),
                (35, ['*']),
                (633, ['#']),
                (617, ['*']),
                (58, []),
                (592, ['+']),
                (755, ['*']),
                (664, ['$']),
                (598, ['*']),
            ]
        )

    def test_all_numbers(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.all_numbers == [
            day_03.EngineSchematicNumber(y=0, x=0, length=3, value=467),
            day_03.EngineSchematicNumber(y=0, x=5, length=3, value=114),
            day_03.EngineSchematicNumber(y=2, x=2, length=2, value=35),
            day_03.EngineSchematicNumber(y=2, x=6, length=3, value=633),
            day_03.EngineSchematicNumber(y=4, x=0, length=3, value=617),
            day_03.EngineSchematicNumber(y=5, x=7, length=2, value=58),
            day_03.EngineSchematicNumber(y=6, x=2, length=3, value=592),
            day_03.EngineSchematicNumber(y=7, x=6, length=3, value=755),
            day_03.EngineSchematicNumber(y=9, x=1, length=3, value=664),
            day_03.EngineSchematicNumber(y=9, x=5, length=3, value=598),
        ]

    def test_part_numbers(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.part_numbers == [
            number
            for number
            in schematic.all_numbers
            if number not in [
                day_03.EngineSchematicNumber(y=0, x=5, length=3, value=114),
                day_03.EngineSchematicNumber(y=5, x=7, length=2, value=58),
            ]
        ]

    def test_all_stars(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.all_stars == [
            day_03.SpecialField(x=3, y=1, length=1),
            day_03.SpecialField(x=3, y=4, length=1),
            day_03.SpecialField(x=5, y=8, length=1),
        ]

    def test_gears(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.gears == [
            day_03.Gear(x=3, y=1, length=1, parts=(
                day_03.EngineSchematicNumber(x=0, y=0, length=3, value=467),
                day_03.EngineSchematicNumber(x=2, y=2, length=2, value=35),
            )),
            day_03.Gear(x=5, y=8, length=1, parts=(
                day_03.EngineSchematicNumber(x=6, y=7, length=3, value=755),
                day_03.EngineSchematicNumber(x=5, y=9, length=3, value=598),
            )),
        ]
        assert [
                   gear.gear_ratio
                   for gear
                   in schematic.gears
               ] == [
                   16345,
                   451490,
               ]

    def test_part_sum(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.part_sum == 4361

    def test_gear_ratio_sum(self):
        schematic = day_03.EngineSchematic.parse(example_lines_1)
        assert schematic.gear_ratio_sum == 467835


def test_solution_1():
    input_lines: Final = input_for(day_03)
    schematic = day_03.EngineSchematic.parse(input_lines)
    assert len(schematic.all_numbers) == 1192
    assert len(schematic.part_numbers) == 1038
    assert schematic.part_sum == 522726


def test_solution_2():
    input_lines: Final = input_for(day_03)
    schematic = day_03.EngineSchematic.parse(input_lines)
    assert len(schematic.all_stars) == 342
    assert len(schematic.gears) == 308
    assert schematic.gear_ratio_sum == 81721933


if __name__ == "__main__":
    run_all_tests()
