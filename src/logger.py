import logging
import sys
from pathlib import Path
from typing import Final

formatter = logging.Formatter('%(name)s: %(message)s')

stderr_handler: Final = logging.StreamHandler(sys.stderr)
stderr_handler.setLevel(logging.WARNING)
stderr_handler.setFormatter(formatter)

stdout_handler: Final = logging.StreamHandler(sys.stdout)
stdout_handler.setLevel(logging.DEBUG)
stdout_handler.addFilter(lambda record: record.levelno < stderr_handler.level)
stdout_handler.setFormatter(formatter)


def create_default_logger(file_path: str) -> logging.Logger:
    logger: Final = logging.getLogger(Path(file_path).stem)
    logger.level = logging.INFO
    logger.addHandler(stdout_handler)
    logger.addHandler(stderr_handler)

    return logger
