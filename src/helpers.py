import sys
from pathlib import Path
from types import ModuleType
from typing import Final

import pytest

root_dir: Final = Path(__file__).parent.parent
input_dir: Final = root_dir.joinpath('input')


def input_for(day: ModuleType) -> list[str]:
    input_file_path: Final = input_dir.joinpath(f"{day.__name__}.txt")
    with open(input_file_path, 'r') as input_file:
        return [
            line.removesuffix('\n')
            for line
            in input_file.readlines()
        ]


def run_all_tests() -> None:
    sys.exit(pytest.main([
        *sys.argv,
        '--color=yes',
        '-vv',
    ]))
